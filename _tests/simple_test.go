package _tests

import (
	"gitlab.com/remeex/godaser/serializable"
	"gitlab.com/remeex/godaser/types"
	"gitlab.com/remeex/gomessages/message"
	"gitlab.com/remeex/gomessages/transport/tcp"
	"gitlab.com/remeex/goremeex/method"
	"gitlab.com/remeex/goremeex/server"
	"gitlab.com/remeex/goremeex/util"
	"strings"
	"testing"
)

type DStringArray struct {
	list []*types.DString
}

func (arr *DStringArray) Serialize(out *serializable.Serializer) error {
	if err := out.WriteInt(len(arr.list)); err != nil {
		return err
	}
	for _, it := range arr.list {
		if err := it.Serialize(out); err != nil {
			return err
		}
	}
	return nil
}

func (arr *DStringArray) Deserialize(in *serializable.Deserializer) error {
	if l, err := in.ReadInt(); err != nil {
		return err
	} else {
		arr.list = make([]*types.DString, l)
		for i := range arr.list {
			it := new(types.DString)
			if err := it.Deserialize(in); err != nil {
				return err
			}
			arr.list[i] = it
		}
		return nil
	}
}

func NewDStringArray(elements ...string) *DStringArray {
	list := make([]*types.DString, len(elements))
	for i, it := range elements {
		list[i] = &types.DString{Value: it}
	}
	return &DStringArray{list: list}
}

type SimpleService struct {
	ms message.Service
}

func (ss *SimpleService) Join(arg *DStringArray) (result *types.DString, err error) {
	result = new(types.DString)
	err = method.New(123456, result, arg).Execute(ss.ms)
	return
}

func (ss *SimpleService) Close() error {
	err1 := method.Close().Execute(ss.ms)
	err2 := ss.ms.Close()
	return util.CombineErrors(err1, err2)
}

type SimpleClient struct {
	*server.RemoteClient
}

type SimpleHandler struct {
	OnClose server.CloseCallbackType
	OnJoin  func(arg *DStringArray) (*types.DString, error)
}

func (sh *SimpleHandler) Accept(acceptor message.ServiceAcceptor) (*SimpleClient, error) {
	h := &server.Handler{
		CloseCallback: sh.OnClose,
		MethodCallbacks: map[int64]server.MethodCallbackType{
			123456: func(args message.Message, result *serializable.Serializable) error {
				arg1 := new(DStringArray)
				remoteArgs := method.NewArguments(arg1)
				if err := remoteArgs.FromMessage(args); err != nil {
					return err
				}
				callbackResult, err := sh.OnJoin(arg1)
				if err == nil {
					*result = callbackResult
				}
				return err
			},
		},
	}
	rc, err := h.Accept(acceptor)
	return &SimpleClient{rc}, err
}

func startServer() (message.ServiceAcceptor, error) {
	return tcp.NewAcceptor(8080)
}

func connectToServer() (message.Service, error) {
	return tcp.NewClient("127.0.0.1", 8080)
}

func TestSimple(t *testing.T) {
	acceptor, err := startServer()
	if err != nil {
		t.Log("cannot start server on port 8080>", err.Error())
		return
	}

	successServer := false
	waitServer := true

	go func() {
		defer func() {
			waitServer = false
		}()
		handler := &SimpleHandler{
			OnClose: func() error {
				t.Log("OnClose")
				return nil
			},
			OnJoin: func(arg *DStringArray) (*types.DString, error) {
				list := make([]string, len(arg.list))
				for i, it := range arg.list {
					list[i] = it.Value
				}
				return &types.DString{Value: strings.Join(list, "")}, nil
			},
		}
		client, err := handler.Accept(acceptor)
		if err != nil {
			t.Log("accept error>", err.Error())
			return
		} else {
			t.Log("connected to", client.GetAddr().String())
		}

		if err := client.Run(); err != nil {
			t.Log("remote client error>", err.Error())
			return
		}

		if err := client.Close(); err != nil {
			t.Log("remote client close error>", err.Error())
			return
		}

		successServer = true
	}()

	ms, err := connectToServer()
	if err != nil {
		acceptor.Close()
		t.Fatal("cannot connect to server>", err.Error())
		return
	}

	service := &SimpleService{ms}

	joined, err := service.Join(NewDStringArray("foo", ";", "bar"))
	if err != nil {
		service.Close()
		acceptor.Close()
		t.Fatal("cannot execute Join method>", err.Error())
		return
	}

	service.Close()

	if joined.Value != "foo;bar" {
		acceptor.Close()
		t.Fatal("invalid Join result> expected \"foo;bar\", actual \"" + joined.Value + "\"")
		return
	}

	for waitServer {
		if !waitServer {
			break
		}
	}

	acceptor.Close()

	if !successServer {
		t.Fatal("unsuccessful server")
	}
}

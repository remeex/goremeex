package util

type CombinedError struct {
	First, Second error
}

func (ce *CombinedError) Error() string {
	return ce.First.Error() + "\n" + ce.Second.Error()
}

func combine(a, b error) error {
	if a == nil {
		return b
	}
	if b == nil {
		return a
	}
	return &CombinedError{First: a, Second: b}
}

func CombineErrors(a error, b ...error) error {
	result := a
	for _, it := range b {
		result = combine(result, it)
	}
	return result
}

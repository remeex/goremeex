module gitlab.com/remeex/goremeex

go 1.15

require (
	gitlab.com/remeex/godaser v1.0.1
	gitlab.com/remeex/gomessages v1.0.1
)

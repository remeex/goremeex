package method

import (
	"errors"
	"gitlab.com/remeex/godaser/serializable"
	"gitlab.com/remeex/gomessages/message"
)

type Result struct {
	Value serializable.Serializable
	Error error
}

func (r *Result) Serialize(out *serializable.Serializer) error {
	var err error
	if r.Error != nil {
		err = out.WriteBool(true)
		if err == nil {
			err = out.WriteString(r.Error.Error())
		}
	} else {
		err = out.WriteBool(false)
		if err == nil && r.Value != nil {
			err = r.Value.Serialize(out)
		}
	}
	return err
}

func (r *Result) Deserialize(in *serializable.Deserializer) error {
	if hasError, err := in.ReadBool(); err != nil {
		return err
	} else if hasError {
		if msg, err := in.ReadString(); err != nil {
			return err
		} else {
			r.Error = errors.New(msg)
		}
	} else if r.Value != nil {
		return r.Value.Deserialize(in)
	}
	return nil
}

func (r *Result) ToMessage() (message.Message, error) {
	return message.FromSerializable(r)
}

func (r *Result) FromMessage(m message.Message) error {
	return m.ToSerializable(r)
}

package method

import (
	"gitlab.com/remeex/godaser/serializable"
	"gitlab.com/remeex/gomessages/message"
	"gitlab.com/remeex/goremeex/util"
)

type RemoteMethod struct {
	Head *Header
	Args *Arguments
	Res  *Result
}

func (rm *RemoteMethod) Execute(s message.Service) error {
	m, err := rm.Head.ToMessage()
	if err != nil {
		return err
	}
	if err = s.SendMessage(m); err != nil {
		return err
	}
	m, err = rm.Args.ToMessage()
	if err != nil {
		return err
	}
	if err = s.SendMessage(m); err != nil {
		return err
	}
	m, err = s.ReceiveMessage()
	if err != nil {
		return err
	}
	err = rm.Res.FromMessage(m)
	return util.CombineErrors(err, rm.Res.Error)
}

func newMethod(t Type, id int64,
	result serializable.Serializable,
	args []serializable.Serializable) *RemoteMethod {
	return &RemoteMethod{
		Head: &Header{Type: t, Id: id},
		Args: &Arguments{List: args},
		Res:  &Result{Value: result, Error: nil},
	}
}

func Close() *RemoteMethod {
	return newMethod(TypeClose, 0, nil, nil)
}

func New(id int64, result serializable.Serializable, args ...serializable.Serializable) *RemoteMethod {
	return newMethod(TypeService, id, result, args)
}

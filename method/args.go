package method

import (
	"gitlab.com/remeex/godaser/serializable"
	"gitlab.com/remeex/gomessages/message"
)

type Arguments struct {
	List []serializable.Serializable
}

func (a *Arguments) Serialize(out *serializable.Serializer) error {
	if a.List != nil {
		for _, it := range a.List {
			if err := it.Serialize(out); err != nil {
				return err
			}
		}
	}
	return nil
}

func (a *Arguments) Deserialize(in *serializable.Deserializer) error {
	if a.List != nil {
		for _, it := range a.List {
			if err := it.Deserialize(in); err != nil {
				return err
			}
		}
	}
	return nil
}

func (a *Arguments) ToMessage() (message.Message, error) {
	return message.FromSerializable(a)
}

func (a *Arguments) FromMessage(m message.Message) error {
	return m.ToSerializable(a)
}

func NewArguments(args ...serializable.Serializable) *Arguments {
	return &Arguments{List: args}
}

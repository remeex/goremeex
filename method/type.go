package method

import (
	"gitlab.com/remeex/godaser/serializable"
)

type Type byte

const (
	TypeNone Type = iota
	TypeClose
	TypeService
)

func (t *Type) Serialize(out *serializable.Serializer) error {
	return out.WriteByte(byte(*t))
}

func (t *Type) Deserialize(in *serializable.Deserializer) error {
	return in.ReadBytePtr((*byte)(t))
}

package method

import (
	"gitlab.com/remeex/godaser/serializable"
	"gitlab.com/remeex/gomessages/message"
)

type Header struct {
	Type Type
	Id   int64
}

func (h *Header) Serialize(out *serializable.Serializer) error {
	if err := h.Type.Serialize(out); err != nil {
		return err
	}
	return out.WriteInt64(h.Id)
}

func (h *Header) Deserialize(in *serializable.Deserializer) error {
	if err := h.Type.Deserialize(in); err != nil {
		return err
	}
	return in.ReadInt64Ptr(&h.Id)
}

func (h *Header) ToMessage() (message.Message, error) {
	return message.FromSerializable(h)
}

func (h *Header) FromMessage(m message.Message) error {
	return m.ToSerializable(h)
}

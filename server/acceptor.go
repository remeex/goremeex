package server

import (
	"gitlab.com/remeex/gomessages/message"
)

func (h *Handler) Accept(acceptor message.ServiceAcceptor) (*RemoteClient, error) {
	client, err := acceptor.Accept()
	if err != nil {
		return nil, err
	}
	return &RemoteClient{ms: client, h: h}, nil
}

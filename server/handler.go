package server

import (
	"errors"
	"gitlab.com/remeex/godaser/serializable"
	"gitlab.com/remeex/gomessages/message"
	"gitlab.com/remeex/goremeex/method"
	"gitlab.com/remeex/goremeex/util"
	"strconv"
)

type MethodCallbackType func(args message.Message, result *serializable.Serializable) error
type CloseCallbackType func() error

type Handler struct {
	MethodCallbacks map[int64]MethodCallbackType
	CloseCallback   CloseCallbackType
}

func (h *Handler) runImpl(service message.Service, result *method.Result, run *bool) error {
	m, err := service.ReceiveMessage()
	if err != nil {
		result.Error = err
		return err
	}
	header := new(method.Header)
	if err = header.FromMessage(m); err != nil {
		result.Error = err
		return err
	}

	switch header.Type {
	case method.TypeClose, method.TypeService:
		m, err = service.ReceiveMessage()
		if err != nil {
			result.Error = err
			return err
		}
	default:
		result.Error = errors.New("cannot execute None method '" +
			strconv.FormatInt(header.Id, 10) + "'")
		return result.Error
	}

	if header.Type == method.TypeClose {
		*run = false
		if h.CloseCallback != nil {
			if err = h.CloseCallback(); err != nil {
				result.Error = err
				return err
			}
		}
	} else /*TypeService*/ {
		if callback, has := h.MethodCallbacks[header.Id]; has {
			if err = callback(m, &result.Value); err != nil {
				result.Error = err
				return err
			}
		} else {
			result.Error = errors.New("unimplemented service method '" +
				strconv.FormatInt(header.Id, 10) + "'")
			return result.Error
		}
	}

	return nil
}

func (h *Handler) run(service message.Service) error {
	run := true
	for run {
		result := new(method.Result)
		runErr := h.runImpl(service, result, &run)
		if m, err := result.ToMessage(); err != nil {
			return util.CombineErrors(runErr, err)
		} else if err = service.SendMessage(m); err != nil {
			return util.CombineErrors(runErr, err)
		} else if runErr != nil {
			return runErr
		}
	}
	return nil
}

package server

import (
	"gitlab.com/remeex/gomessages/message"
	"net"
)

type RemoteClient struct {
	ms message.Service
	h  *Handler
}

func (r *RemoteClient) Close() error {
	return r.ms.Close()
}

func (r *RemoteClient) GetAddr() net.Addr {
	return r.ms.GetAddr()
}

func (r *RemoteClient) Run() error {
	return r.h.run(r.ms)
}
